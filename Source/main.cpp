//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>

class MidiMessage{
public:
    MidiMessage()//Constructor
    {number = 60;
        velo = 0;
        channelNum = 1;
    }
    ~MidiMessage()//Destructor
    {   }
    
    
    void SetNoteNumber (int value) //Mutator
    {
        if(value>=0)
            number = value;
    }
    void SetVelocity (int value) //Mutator
    {
        if(value<=127 && value>=0)
            velo = value;
    }
    void SetChannelNumber (int value) //Mutator
    {
        if (value>=0)
            channelNum = value;
    }
    
    
    int getNoteNumber() const //Accessor
    {
        return number;
    }
    int getVelocity() const //Accessor
    {
        return velo;
    }
    int getChannelNum() const //Accessor
    {
        return channelNum;
    }
    
    
    float getMidiNoteInHertz() const //Accessor
    {
        return 440.0*pow(2.0, (number-69.0)/12.0);
    }
    float getFloatAmp() const //Accessor
    {
        return velo/127.0;
    }
private:
    int number;
    int velo;
    int channelNum;
};
int main (int argc, const char* argv[])
{
    int val;
    MidiMessage note;
    std::cout<<"Please enter MIDI Number\n";
    std::cin>>val;
    note.SetNoteNumber(val);
    std::cout<<"Please enter Velocity value\n";
    std::cin>>val;
    note.SetVelocity(val);
    std::cout<<"Please enter Channel Number\n";
    std::cin>>val;
    note.SetChannelNumber(val);
    std::cout<<"Note number is " << note.getNoteNumber()<<"\n";
    std::cout<<"Note frequency is " << note.getMidiNoteInHertz()<<"\n\n";
    std::cout<<"Velocity is " << note.getVelocity()<<"\n";
    std::cout<<"Amplitude is " << note.getFloatAmp()<<"\n";
    std::cout<<"Channel number is " << note.getChannelNum()<<"\n";
    
    
    
    /* insert code here...
     std::cout << "Hello, World!\n";*/
    return 0;
}

